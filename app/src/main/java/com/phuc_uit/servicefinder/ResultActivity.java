package com.phuc_uit.servicefinder;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class ResultActivity extends AppCompatActivity {
    ResultPageAdapter mAdapter;
    ViewPager mViewPager;
    TabLayout mTlMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mAdapter = new ResultPageAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.vp_main);
        mViewPager.setAdapter(mAdapter);

        mTlMain = findViewById(R.id.tb_main);
        mTlMain.setupWithViewPager(mViewPager);
        mTlMain.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}