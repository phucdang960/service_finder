package com.phuc_uit.servicefinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private final String TAG = "sf-log";

    private View mViewDistricts;
    private View mViewDistance;

    private List<String> mServices;
    private List<String> mArea;
    private List<String> mDistricts;
    private List<String> mSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initData();
        initView();
    }

    private void initData(){
        //Init service data
        mServices = new ArrayList<>();
        mServices.add("Cây xăng");
        mServices.add("Cửa hàng");
        mServices.add("Quán cafe");

        //Init area data
        mArea = new ArrayList<>();
        mArea.add("Vị trí hiện tại");
        mArea.add("Hồ Chí Minh");
        mArea.add("Hà Nội");

        //Init district
        mDistricts = new ArrayList<>();
        mDistricts.add("Quận 1");
        mDistricts.add("Quận 2");
        mDistricts.add("Quận 3");
        mDistricts.add("Thủ Đức");

        mSort = new ArrayList<>();
        mSort.add("Khoảng cách");
        mSort.add("A-Z");
        mSort.add("Z-A");

    }

    private void initView(){
        mViewDistance = findViewById(R.id.view_distance);
        mViewDistricts = findViewById(R.id.view_district);

        //Initialize service
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                mServices);
        Spinner spinner = findViewById(R.id.sp_services);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG,"Selected position " + position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG,"Selected nothing ");
            }
        });

        //Initialize area
        adapter =  new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                mArea);
        spinner = findViewById(R.id.sp_area);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    mViewDistance.setVisibility(View.VISIBLE);
                    mViewDistricts.setVisibility(View.GONE);
                } else{
                    mViewDistance.setVisibility(View.GONE);
                    mViewDistricts.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Initialize district
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                mDistricts);
        spinner = findViewById(R.id.sp_district);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Initialize sort
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                mSort);
        spinner = findViewById(R.id.sp_sort);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Register event click button
        Button search = findViewById(R.id.btn_seach);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this,ResultActivity.class);
                startActivity(intent);
            }
        });
    }
}
