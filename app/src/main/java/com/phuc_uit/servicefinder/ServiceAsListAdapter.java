package com.phuc_uit.servicefinder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class ServiceAsListAdapter extends RecyclerView.Adapter<ServiceAsListAdapter.ServiceHolder> {
    public class ServiceHolder extends RecyclerView.ViewHolder{
        public ServiceHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(Service service){

        }
    }

    private List<Service> mServices = null;
    private Context mContext = null;

    public ServiceAsListAdapter(Context context,List<Service> services) {
        super();
        mContext = context;
        mServices = services;
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_service,viewGroup,false);
        return new ServiceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHolder serviceHolder, int i) {
        serviceHolder.bind(mServices.get(i));
    }

    @Override
    public int getItemCount() {
        return mServices != null?mServices.size():0;
    }
}
